"""
     Module: Statistical Modules
    Project: Sparx
    Authors: Vandana Bhagat
     Email : vandana.bhagat@cleverinsight.co
"""
from __future__ import division
import numpy as np

def pearson(arr1, arr2):
    ''' Returns the corelation result of two arrays
    Parameters:
    -----------
        x: Enter first array with numeric  values
        y: Enter second array with numeric values
    Usage:
    ------
        >>> pearson([17,13,12,15,16,14,16,16,18,19],\
        ... [94,73,59,80,93,85,66,79,77,91])
        >>> 0.5960
    '''
    arr1_ = np.mean(arr1)
    arr2_ = np.mean(arr2)

    x_y_sum, x_x_sq, y_y_sq = [], [], []

    for i in xrange(len(arr1)):
        x_y_sum.append((arr1[i] - arr1_) * (arr2[i] - arr2_))
        x_x_sq.append((arr1[i]-arr1_)**2)
        y_y_sq.append((arr2[i]-arr2_)**2)
    return np.sum(x_y_sum) / np.sqrt(np.sum(x_x_sq) * np.sum(y_y_sq))

def spearman(list1, list2):
    ''' Returns the corelation result of two arrays
    Parameters:
    -----------
        x: Enter first array with numeric  values
        y: Enter second array with numeric values
    Usage:
    ------
        >>> Spearman([73, 76, 78, 65, 86, 82, 91],\
        ... [77, 78, 79, 80, 86, 89, 95])
        >>> 75.0
    '''
    col = [list(a) for a in zip(list1, list2)]
    rank_list1 = sorted(col, key=lambda list1: list1[0], reverse=True)
    for i, row in enumerate(rank_list1):
        # appending x_ value
        row.append(i+1)
    rank_list2 = sorted(rank_list1, key=lambda list1: list1[1], reverse=True)
    for i, row in enumerate(rank_list2):
        # appending y_ value
        row.append(i+1)

        # appending d = (x_ - y_)
        row.append(row[2] - row[3])

        # appending (x_ - y_)^2
        row.append((row[2] - row[3]) ** 2)

    rank_list2 = np.array(rank_list2)
    values = len(rank_list2)
    sum_list2 = np.sum(rank_list2[:, 5])
    return (1 - (6*sum_list2) / (values*(values**2 - 1)))*100


__all__ = [
    'pearson',
    'spearman'
]
