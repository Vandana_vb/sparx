from sparx.stats import *
import numpy as np

def test_pearson():
	x = np.array([17,13,12,15,16,14,16,16,18,19])
	y = np.array([94,73,59,80,93,85,66,79,77,91])
	assert pearson(x, y) == 0.59609476138946238

def test_spearman():
	list1 = [73, 76, 78, 65, 86, 82, 91]
	list2 = [77, 78, 79, 80, 86, 89, 95]
	assert spearman(list1, list2) == 75.0



