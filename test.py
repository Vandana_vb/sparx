from __future__ import division
from sparx.stats import *
import pandas as pd
import numpy as np

#from .preprocess import *



x=np.array([17,13,12,15,16,14,16,16,18,19])
y=np.array([94,73,59,80,93,85,66,79,77,91])

list1 = [73, 76, 78, 65, 86, 82, 91]
list2 = [77, 78, 79, 80, 86, 89, 95]

query = {'name': 'Sam', 'age': 20 }
data = pd.read_csv('data/iris.csv')
series = pd.Series(['Curd ', 'GOOG APPL MS', 'A B C', 'T Test is'])
df = pd.DataFrame(np.array([
                    [1,1,0],
                    [2,10,50000],
                    [1,14,10],
                    [3,100,11],
                    [10,1400,11],
                    [50,10,13],
                    [100,11,14],
                    [101,91,18],
                    [111,16,19],
                    [300,13,1],
                    [1000,1,1],
            ]))

# print unique_identifier(data)
#print date_split("march/1/1980")
#print count_missing(data)
#print dict_query_string({'name': 'Sam', 'age': 20 })
#print describe(data, "Sepal.Length")
#print encode(data)
#print has_keywords(series, sep=' ', thresh=2)
#print missing_percent(data["Sepal.Width"])
#print missing_percent(data)
#print strip_non_alphanum("epqenw49021[4;;ds..,.,uo]mfLCP'X'") 
#print word_freq_count("hello how are you")
#print ignore_stopwords("I am basically a lazy person and i hate computers")
#print ignore_outlier(df)
#print is_categorical(data["Species"])
#print  is_date(pd.Series(['Jul 31, 2009', '2010-01-10', None]))
#print  is_date(pd.Series(['Jul 31, 2009', '2010-101-10', None]))

print pearson(x,y)
print spearman(list1, list2)